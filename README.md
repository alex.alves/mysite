TODO:
To extend this MVP, you can:

Add more themes and articles to the blog section
Include images and more detailed styling
Implement a backend API to fetch blog posts and resume files
Add a contact form
Include social media links
Add a search functionality for blog posts
Implement dark mode support
Add animations and transitions
Include a portfolio section with your projects