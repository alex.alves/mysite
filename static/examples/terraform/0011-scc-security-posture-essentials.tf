locals {
    organization_id = "000000000" # Update with correct one

}

resource "google_securityposture_posture_deployment" "scc-sec-posture-deployment" {
  posture_deployment_id = "scc-posture-deployment-poc"
  parent                = "organizations/${local.organization_id}"
  location              = "global"
  description           = "Security Command Center - Poc"
  target_resource       = var.folder_ids["scc-poc"]
  posture_id            = google_securityposture_posture.scc-sec-posture-tf-poc.name
  posture_revision_id   = google_securityposture_posture.scc-sec-posture-tf-poc.revision_id
}

resource "google_securityposture_posture" "scc-sec-posture-tf-poc" {
  posture_id  = "sec-posture-tf"
  parent      = "organizations/${local.organization_id}"
  location    = "global"
  state       = "ACTIVE"
  description = "A new posture for testing"
  policy_sets {
    policy_set_id = "org_policy_set"
    description   = "set of org policies"

    policies {
      policy_id   = "Restrict shared VPC project lien removal"
      description = "Prevent the accidental deletion of Shared VPC host projects by restricting the removal of project liens."
      compliance_standards {
        standard = "NIST SP 800-53"
        control  = "AC-3"
      }
      compliance_standards {
        standard = "NIST SP 800-53"
        control  = "AC-6"
      }

      constraint {
        org_policy_constraint {
          canned_constraint_id = "compute.restrictXpnProjectLienRemoval"
          policy_rules {
            enforce = true
          }

        }
      }

    }

    policies {
      policy_id   = "Define allowed external IPs for VM instances"
      description = "Prevent the creation of Compute instances with a public IP, which can expose them to internet ingress and egress."
      compliance_standards {
        standard = "NIST SP 800-53"
        control  = "AC-3"
      }
      compliance_standards {
        standard = "NIST SP 800-53"
        control  = "AC-6"
      }

      constraint {
        org_policy_constraint {
          canned_constraint_id = "compute.vmExternalIpAccess"
          policy_rules {
            deny_all = true
          }

        }
      }

    }

    policies {
      policy_id   = "Skip default network creation"
      description = "Disable the automatic creation of a default VPC network and default firewall rules in each new project, ensuring that my network and firewall rules are intentionally created."
      compliance_standards {
        standard = "NIST SP 800-53"
        control  = "AC-3"
      }
      compliance_standards {
        standard = "NIST SP 800-53"
        control  = "AC-6"
      }

      constraint {
        org_policy_constraint {
          canned_constraint_id = "compute.skipDefaultNetworkCreation"
          policy_rules {
            enforce = true
          }

        }
      }
    }

    policies {
      policy_id   = "Sets the internal DNS setting for new projects to Zonal DNS Only"
      description = "Set guardrails that application developers cannot choose legacy DNS settings for compute instances that have lower service reliability than modern DNS settings."
      compliance_standards {
        standard = "NIST SP 800-53"
        control  = "AC-3"
      }
      compliance_standards {
        standard = "NIST SP 800-53"
        control  = "AC-6"
      }

      constraint {
        org_policy_constraint {
          canned_constraint_id = "compute.setNewProjectDefaultToZonalDNSOnly"
          policy_rules {
            enforce = true
          }

        }
      }

    }

    policies {
      policy_id   = "Restrict Public IP access on Cloud SQL instances"
      description = "Prevent the creation of Cloud SQL instances with a public IP, which can expose them to internet ingress and egress."
      compliance_standards {
        standard = "NIST SP 800-53"
        control  = "AC-3"
      }
      compliance_standards {
        standard = "NIST SP 800-53"
        control  = "AC-6"
      }

      constraint {
        org_policy_constraint {
          canned_constraint_id = "sql.restrictPublicIp"
          policy_rules {
            enforce = true
          }

        }
      }

    }

    policies {
      policy_id   = "Restrict Authorized Networks on Cloud SQL instances"
      description = "Prevent public or non-RFC 1918 network ranges from accessing my Cloud SQL databases."
      compliance_standards {
        standard = "NIST SP 800-53"
        control  = "AC-3"
      }
      compliance_standards {
        standard = "NIST SP 800-53"
        control  = "AC-6"
      }

      constraint {
        org_policy_constraint {
          canned_constraint_id = "sql.restrictAuthorizedNetworks"
          policy_rules {
            enforce = true
          }

        }
      }

    }


    policies {
      policy_id   = "Restrict Protocol Forwarding Based on type of IP Address"
      description = "Allow VM protocol forwarding for internal IP addresses only."
      compliance_standards {
        standard = "NIST SP 800-53"
        control  = "AC-3"
      }
      compliance_standards {
        standard = "NIST SP 800-53"
        control  = "AC-6"
      }

      constraint {
        org_policy_constraint {
          canned_constraint_id = "compute.restrictProtocolForwardingCreationForTypes"
          policy_rules {
            values {
              allowed_values = ["INTERNAL"]
            }
          }

        }
      }
    }

    policies {
      policy_id   = "Disable VPC External IPv6 usage"
      description = "Prevent the creation of external IPv6 subnets, which can be exposed to internet ingress and egress."
      compliance_standards {
        standard = "NIST SP 800-53"
        control  = "AC-3"
      }
      compliance_standards {
        standard = "NIST SP 800-53"
        control  = "AC-6"
      }

      constraint {
        org_policy_constraint {
          canned_constraint_id = "compute.disableVpcExternalIpv6"
          policy_rules {
            enforce = true
          }

        }
      }
    }

    policies {
      policy_id   = "Disable service account key creation"
      description = "Prevent users from creating persistent keys for service accounts to decrease the risk of exposed service account credentials."

      compliance_standards {
        standard = "NIST SP 800-53"
        control  = "AC-2"
      }

      constraint {
        org_policy_constraint {
          canned_constraint_id = "iam.disableServiceAccountKeyCreation"
          policy_rules {
            enforce = true
          }

        }
      }
    }
    policies {
      policy_id   = "Disable Automatic IAM Grants for Default Service Accounts"
      description = "Prevent default service accounts from receiving the overly-permissive IAM role Editor at creation."

      compliance_standards {
        standard = "NIST SP 800-53"
        control  = "AC-3"
      }

      constraint {
        org_policy_constraint {
          canned_constraint_id = "iam.automaticIamGrantsForDefaultServiceAccounts"
          policy_rules {
            enforce = true
          }

        }
      }
    }
    policies {
      policy_id   = "Disable Service Account Key Upload"
      description = "Avoid the risk of leaked and reused custom key material in service account keys."
      compliance_standards {
        standard = "NIST SP 800-53"
        control  = "AC-6"
      }

      constraint {
        org_policy_constraint {
          canned_constraint_id = "iam.disableServiceAccountKeyUpload"
          policy_rules {
            enforce = true
          }

        }
      }

    }
    policies {
      policy_id   = "Enforce Public Access Prevention"
      description = "Enforce that Storage Buckets cannot be configured as open to unauthenticated public access."
      compliance_standards {
        standard = "NIST SP 800-53"
        control  = "AC-3"
      }
      compliance_standards {
        standard = "NIST SP 800-53"
        control  = "AC-6"
      }

      constraint {
        org_policy_constraint {
          canned_constraint_id = "storage.publicAccessPrevention"
          policy_rules {
            enforce = true
          }

        }
      }
    }

    policies {
      policy_id   = "Enforce uniform bucket-level access"
      description = "Prevent GCS buckets from using per-object ACL (a separate system from IAM policies) to provide access, enforcing a consistency for access management and auditing."
      compliance_standards {
        standard = "NIST SP 800-53"
        control  = "AC-3"
      }
      compliance_standards {
        standard = "NIST SP 800-53"
        control  = "AC-6"
      }

      constraint {
        org_policy_constraint {
          canned_constraint_id = "storage.uniformBucketLevelAccess"
          policy_rules {
            enforce = true
          }

        }
      }
    }

    policies {
      policy_id   = "Require OS Login"
      description = "Require OS Login on newly created VMs to more easily manage SSH keys, provide resource-level permission with IAM policies, and log user access."
      compliance_standards {
        standard = "NIST SP 800-53"
        control  = "AC-3"
      }
      compliance_standards {
        standard = "NIST SP 800-53"
        control  = "AU-12"
      }

      constraint {
        org_policy_constraint {
          canned_constraint_id = "compute.requireOsLogin"
          policy_rules {
            enforce = true
          }

        }
      }

    }

    policies {
      policy_id   = "Disable VM serial port access"
      description = "Prevent users from accessing the VM serial port which can be used for backdoor access from the Compute Engine API control plane"
      compliance_standards {
        standard = "NIST SP 800-53"
        control  = "AC-3"
      }
      compliance_standards {
        standard = "NIST SP 800-53"
        control  = "AC-6"
      }

      constraint {
        org_policy_constraint {
          canned_constraint_id = "compute.disableSerialPortAccess"
          policy_rules {
            enforce = true
          }

        }
      }
    }

    policies {
      policy_id   = "Disable VM nested virtualization"
      description = "Disable nested virtualization to decrease my security risk due to unmonitored nested instances."
      compliance_standards {
        standard = "NIST SP 800-53"
        control  = "AC-3"
      }
      compliance_standards {
        standard = "NIST SP 800-53"
        control  = "AC-6"
      }
      constraint {
        org_policy_constraint {
          canned_constraint_id = "compute.disableNestedVirtualization"
          policy_rules {
            enforce = true
          }
        }
      }
    }
  }
}