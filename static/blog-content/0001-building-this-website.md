**UPDATE** This content is no longer valid to the existing version of this site.

After quite a while of thinking and tickering, I've decided to create my own personal website. At the end of the day, I came across [hugo](<https://gohugo.io/>) software via a friend website. Hugo declares to be, and now I'm paraphrasing here, "The world’s fastest framework for building websites". If you tried it, it's fast, really fast. Hugo also has a bunch of themes, developed by third-parties, that can be used. Therefore, simplifying even more the creation of the website. It provides a way to add multiple languages to a site.

This whole website is done simply with Hugo + [Toha theme](<https://github.com/hugo-toha/toha>). At the time of this writing, the documentation is a bit spotty with certain parts not being updated to reflect the version. It is still not a major problem as the code for demo site will help in getting things right. [Toha demo site code](<https://github.com/hugo-toha/hugo-toha.github.io>).

The site is hosted in Gitlab Pages which integrates with Let's Encrypt which allows anyone to get a domain to have the website without a browser complaining that it is a unsecure site. 

One of the "issues" I had was how to implement the automatic deployment of the website once I pushed my code to Gitlab. The main configuration file has a variable named baseURL that needs to point to the domain. In my case <https://aalves.co/>. Obviously, when running this code locally, the value is <http://localhost>, but once deployed it needed to be <https://aalves.co>.
Within Gitlab, we can set variables at project level. This variable is then available during the execution of the CI/CD pipeline.

Defining the variable in Gitlab is as simple as opening the **Project -> Settings -> CI/CD** followed by opening the subsection called **variables**. Once the variable is set, it can be accessible via the pipeline code specifed in **.gitlab-ci.yml**.

```bash
image: registry.gitlab.com/pages/hugo:latest

variables:
        GIT_SUBMODULE_STRATEGY: recursive

pages:
  script:
  - sh -c 'sed -i "s/baseURL.*/baseURL\:\ ${WEBPAGE_DOMAIN}/g" config.yaml'
  - hugo
  artifacts:
    paths:
    - public
  only:
  - master
```


**Photo by** [Martin Sanchez](https://unsplash.com/@zekedrone?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText) on [Unsplash](https://unsplash.com/t/experimental?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText)
  