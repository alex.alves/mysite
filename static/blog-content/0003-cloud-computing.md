
## Overview and definition
The Cloud has been around for quite a while now, its usage has been growing every year with vendors adding more services to increase their footprint and get ahead of the competition. It is expected that by 2026 the spending will be of 810 billion USD. 


The term cloud is usually a short for Cloud Computing where the three main hyperscalers have defined it as: 

<div class="grid-cols-3">
<div class="col">

### [Google](https://cloud.google.com/learn/what-is-cloud-computing)

“Cloud computing is the on-demand availability of computing resources as services over the internet. It eliminates the need for enterprises to procure, configure, or manage resources themselves, and they only pay for what they use.”

</div>
<div class="col">

### [Microsoft](https://azure.microsoft.com/en-us/resources/cloud-computing-dictionary/what-is-cloud-computing/)

“Simply put, cloud computing is the delivery of computing services—including servers, storage, databases, networking, software, analytics, and intelligence—over the Internet (“the cloud”) to offer faster innovation, flexible resources, and economies of scale. You typically pay only for cloud services you use, helping you lower your operating costs, run your infrastructure more efficiently, and scale as your business needs change.”

</div>
<div class="col">

### [Amazon](https://aws.amazon.com/what-is-cloud-computing/)

“Cloud computing is the on-demand delivery of IT resources over the Internet with pay-as-you-go pricing. Instead of buying, owning, and maintaining physical data centers and servers, you can access technology services, such as computing power, storage, and databases, on an as-needed basis from a cloud provider like Amazon Web Services (AWS).”

</div>
</div>




Although they all use different descriptions, the result is the same, Cloud Computing provides the ability for organization to consume IT resources through internet via a pay-as-you-go model.

## What does Cloud Computing bring to the table

Attached to cloud computing there are some advantages. Obviously there is no magic or miracle with cloud but it does give some very good advantages when compared to the traditional On-premises solutions:
- **Flexibiity and Scalability**: An organization can increase or decrease their resources as needed, example if they need a new server or more storage, they are not required to wait for a 2-6 months lead time for delivery of the equipment, also it is easier to rightsize an environment to utilize the resources that are actually needed on the go.
- **Availability**: An organization can expect that certain services will be available in multiple locations while owning their own datacenters would require at least two and very expensive connections between them.
- **Cost Savings**: As a benefit of Elasticity dropping resources when they are no longer needed or increasing resources for just when they are needed results in a lower expense for the organization as they do not need to purchase hardware that would otherwise not be fully utilized.

## Types of Services

With the cloud a few terms starting to get popular and often there is information on IaaS, PaaS, SaaS, FaaS, CaaS and I won't be surprised is some other buzzword cames along. In this post I will focus solely on IaaS, PaaS and SaaS.
If you are new to cloud, the difference between IaaS, PaaS and SaaS is mainly on what you as a user are responsible for. The image at the end of this post provides an overview.

![Services Types](/images/0003-CloudServicesTypes.png)


---
> **IaaS - Infrastructure as a Service**

First I'll define what is infrastructure. In computing, infrastructure are all components related to hardware and hosting for that hardware, typically datacenters.
Therefore, IaaS is where the vendor handles all of the components, such as, network, physical servers, storage and virtualization and provides them to an user/organization. When and organization deploys a Virtual Machine, the organization is responsible for the OS, applications, data and security, while the vendor is responsible for ensure the underlying components, hypervisor, storage, and network work.

*Examples of IaaS services*:
- Virtal Machines (AWS EC2, Google Compute Engine, Azure Virtual Machine)

---
> **PaaS - Platform as a Service**

Platform adds on top of the IaaS by including the management of the Operating System, databases, middleware, development software. It means that an organization is responsible for developing their application and deploy it, they are not as much as concerned with the underlaying software, such as, patching of the operating systems and/or databases.

*Examples of PaaS services*:
- Azure SQL Server
- Google Cloud SQL
- AWS RDS
- Google App Engine

---
> **SaaS - Software as a Service**

Software as a Service is where the organization "owns" nothing, the vendor is entirely responsible for the application, data management, backups and restore and so on. The organization itself will simply use and might be able to customize the software to its needs. But overall has no say on the roadmap and development of the software.

*Examples of SaaS services*:
- Google Workspace
- Microsoft Office 365
- ServiceNow
---



**Photo by** [Gerd Altmann](https://unsplash.com/@felix_mittermeier?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText) on [Unsplash](https://unsplash.com/photos/WLGHjbC0Cq4?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText)
  