## Introduction

In the ever-evolving world of cloud computing, the ability to balancer traffic and serve it close to your consumers is typically a fundamnetal requirement. Cloud Run, Google Cloud's serverless platform, is a managed services that scales up and down as needed. Combining Cloud Run with a Global Load Balancer, will make sure that your traffic goes to the nearest region of your application consumers.

This post will provide an overview of the architecture and provide you with a ready to deploy Terraform code to create the service across one or more regions.

## Components

As you might already expect, there are multiple components for the solution:
1. Global Load Balancer: This will balance the load across the backends, it will automatically send the traffic to the nearest region where the request originated.
2. Cloud Run: It will be deployed in one or more regions. 
3. Serverless Network Endpoint Group: It is configured in order to allow the Global Load Balancer to reach out to the Cloud run service(s).
4. Cloud Armor: WAF and DDOS protection and is attached to the Global Load Balancer.
5. Service Accounts: Are used by the Cloud Run services with the right amount of privileges.
6. SSL Security Policy: Attached to the Global Load Balancer in order to allow only industry accepted SSL versions.


## How does it looks like?

As we can see, quite simple! Users reach out to the Load Balancer which in turn will select the closest to the user Cloud Run service.

![Architecture Overview](/images/0008-cr_and_lb.png "Architecture Overview")

## Step-by-Step

Looking at the diagram, we can see a number of dependencies.

1. The user is dependent on a Load Balancer to access the service(s)
2. The Load Balancer is dependent on the Cloud Run service(s)
3. IAM privileges needed (not visible in the diagram)

With these identified dependencies, we can extract the steps needed.

### Step 1 - Setup IAM

As we want to focus on the right privileges we will create two accounts:
- Service account used by the Cloud Run service(s), it can be without privileges initially.
- User account(s) for the users with the role run.invoker.

### Step 2 - Deploy Cloud Run Services

In the console, navigate to Cloud Run and create your Cloud Run service.
When creating the container, choose an image of your choice and give the right information, such as, port, environment variables.
Do not require authentication as the this is for public access.
Ensure the allowed ingress is set to **internal and load balancer**.

### Step 3 - Configure Load Balancer

In the console, navigate to Cloud Load Balancer to create your Global Load Balancer.
For the front end add an IP eitheir static or ephemereal. Highly recommend to have a static that will then be associated with your SSL certificate.
For the back end, you will need to specify a serverless backend for each region. If you don't have one, you can create it from there.
And then create.

### Step 4 - Try it out

Open a browser and enter the IP defined in the front end of the load balancer.
You will see your application displayed.

### Things to consider:

- Cloud Armor: Add to increase the security of your application. Cloud Armor is a Web Application Firewall and DDoS protector.
- Identity Aware Proxy (IAP): Offload the need to handle authentication at applicaiton layer and relies on IAP for it. Unsucessful login attemps do not reach the backend services.
- Organization Policy: If you are part of a bigger organization and want to ensure only domain a, b, and c can have principals in your platform; you can apply the  organization constraint **iam.allowedPolicyMemberDomains**. You will then need to have Google Identity or Google Workspace configured that will limit who can access the platform. 

**Note:** Applying the organization policy will result in not being able to apply *AllUsers* and *AllAuthenticatedUsers* to IAM policies. The policy will then need to be waived in order for the Cloud run to be made public.

## Conclusion:

Creating the service is quite simple. By default all Cloud Run services will scale up to 100 instances. 
Having Cloud Run services in different regions behind a load balancer also helps in redirecting the traffic into the closest region for the user.
Cloud Armor helps in protecting the workloads. 

This can also be easily deployed using IaC. See Additional Resources for useful links.

## Additional Resources:

- [Cloud Foundation Toolkit](https://cloud.google.com/foundation-toolkit/)
- [Cloud Foundation Fabric](https://github.com/GoogleCloudPlatform/cloud-foundation-fabric)
- [IaC Example](https://gitlab.com/alex.alves/iac-examples)

 
  
Photo by [Martin Krchnacek](https://unsplash.com/@martinkrchnacek?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText) on [Unsplash](https://unsplash.com/photos/OyoaCpMCR0U?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText)