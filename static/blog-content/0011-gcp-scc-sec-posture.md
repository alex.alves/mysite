## Overview

Following on the topic of the previous post, I'll be focusing now in an alternate way to enforce Organization Policies via Security Command Center Premium.
This could be a good solution for those users that have Security Command Center and also want to apply a pre-defined Security Posture [SCC - Predefined Security Posture](https://cloud.google.com/security-command-center/docs/security-posture-overview#predefined-policy).

## The SCC Promise

The Security Posture allows for more than enforcing Organization Policies, it allows also to make Health Checks for a few services, such as, no password for Cloud SQL instances, or BigQuery datasets are not encrypted with a CMEK for example. Although, I am focusing on Organization Policies on this post, I will be going over what it can do and what it cannot do.

The promise is a Security Posture can be applied at any level, but one and only one Security posture can be applied at a certain level. We can have multiple Security Posture but apply one at organization, one per folder and one per project. Applying the Security Posture has also the promise that we will be able to detect configurations inconsistencies/deviations from the Security Policy and raise a finding in Security Command Center. This is great since violations to the policy are detected immediatly, but is that the reality?

## The real world

Applying the Security Posture at Organization Level will have it cascade down to folders and projects. So far so good, it behaves as expected. What it does not work properly, though is the detection of deviations of the Security Posture.
When a Security Posture is applied at any level, the deviations will only be detected at that level and not subsequent levels. By this I mean, that a Security Posture applied at folder **XPTO** will be enforced for all folder and projects underneath it. If we override an organization constraint specified in a Security Posture, in a children resource, example a project under folder **XPTO**, Security Command Center will not detect that deviation. If we overwrite the same constraint but in **XPTO** folder, Security Command Center will detect and raise a finding.

## Implementation

There are two parts to the implementation of a Security Posture.
  - A Security Posture Definition: it includes all aspects of what the baseline should look like, organization constraints and health analytics (not covered in this post).
  - A deployment of the Security Posture definition: the deployment applies the Security Definition.

### Pre-requisites
The user or service account must have the following role:
- roles/securityposture.admin

The project must have the following APIs enabled
- orgpolicy.googleapis.com
- securityposture.googleapis.com

### Example terraform file for Secure By Default, Essentials.
You can download the terraform file here [terraform-sec-posture-example](/files/examples/terraform/0011-scc-security-posture-essentials.tf)

## Summary

To sum up, Security Command Center Security Posture has a really good potencial to enforce Organization Polices. The detection of deviations from the defined baseline is a really good feature although a bit incomplete from my expectation. Today there is no way to directly detect inconsistencies in the configuration unless custom code is prepared and applied, for example, scanning all folders and projects to see which constraints are overwritten and where.

I am curious to see how this feature will evolve and allow for a more clear implementation and deviations detection.

## References
- [Security Command Center](https://cloud.google.com/security-command-center/docs/)
- [SCC - Predefined Security Postures](https://cloud.google.com/security-command-center/docs/security-posture-overview#predefined-policy)


Photo by [Szymon Shields](https://unsplash.com/@shields_mcmxcix?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash) on [Unsplash](https://unsplash.com/photos/a-man-in-a-pool-TGgXEn6NVLQ?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash)
  