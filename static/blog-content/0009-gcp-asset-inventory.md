## Overview

This month's post is about Google Cloud Asset Inventory.
I love this service for the fact that I can get an overview of my organization easily. Furthermore, it allow its users to make complex queries to identify resources and their relationships.

Depending on the level of Asset Inventory permissions granted, whether at the project, folder, or organization level, one can gain a comprehensive overview of an organization.

## What can Asset Inventory do for me?

The official documentation refers that we can do the following
-. Use a custom query language to search for assets metadata.
-. Export assets metadata to get a historic view of the changes or a change done to assets within a specific timeframe.
-. Enable notifications through real-time notifications.
-. Find who has access to what.

Personally I use very often the custom query language and who has access to what, actually more like "does this user has access to this?"

Asset Inventory has been incredibly useful in several ways. Here's a breakdown of what it has helped me identify:
-. Services Lacking Authentication Mechanisms: It easily allowed me to identify serverless (Cloud Run and Cloud Functions) without the proper authentication mechanism at platform level.
-. Publicly Accessible Virtual Machines: The tool allowed me to determine the number of virtual machines that have a public IP.
-. Default Service Account Usage: I can identify which virtual machines and Cloud Run instances are using the default service account.
-. Shared VPC Deployments: Asset Inventory helped me in pinpointing which VMs are deployed in the shared-vpc."

Note: Those with experience in Azure, will find similarities with Azure Resource Graph.

The queries can be target to specific resources, project, folders or organizations.

## Asset Inventory Queries

Here some examples of queries that helped me and I'll be using this as my place holder for when I need them in the future. Unfortunately, Asset Inventory does not allow you to save your own queries.

### Ingress status for Cloud run
```sql
select value, count(value) as total 
from `run_googleapis_com_Service`, unnest(resource.data.metadata.annotations) as annot 
where annot.key like 'run.googleapis.com/ingress' 
group by value
```

### Ingress status for Cloud Function
```sql
select resource.data.serviceConfig.ingressSettings, count(resource.data.serviceConfig.ingressSettings) as total 
from `cloudfunctions_googleapis_com_Function` 
group by resource.data.serviceConfig.ingressSettings
```

### Unauthenticated services
This query includes as well the [**allAuthenticatedUsers**](https://cloud.google.com/iam/docs/overview#all-authenticated-users) as this applies to users authentication with a Google Account as well, therefore it can be outside your organization.

```sql
WITH
  tbl_bindings AS (
  SELECT
    name,
    members
  FROM
    `IAM_POLICY`,
    UNNEST(iamPolicy.bindings) )
SELECT
  SPLIT(name, '/')[2] as service, count(member)
FROM
  tbl_bindings,
  UNNEST(members) as member
where member like '%allUsers' or member like '%allAuthenticatedUsers'
group by service
```

### List of projects containing VMs under specific folder(s)

```sql
select distinct split(name,'/') [SAFE_OFFSET(4)] as ProjectId from `compute_googleapis_com_Instance`, unnest(ancestors) as an 
where an in ('folders/FOLDER1_NUMBER','folders/FOLDER2_NUMBER') 
```

### List of all Cloud Runs with a VPC Connector

```sql
select name from `run_googleapis_com_Service`, unnest(resource.data.spec.template.metadata.annotations) as annotations 
where annotations.key like '%vpc-access-connector'
```

### List all INGRESS from public internet

Example shows ssh query.


```sql
select * from `compute_googleapis_com_Firewall`, unnest(resource.data.allowed) 
where '22' in unnest(ports) and IPProtocol like 'tcp' and '0.0.0.0/0' in unnest(resource.data.sourceRanges)
```



References:
- [Cloud Asset Inventory](https://cloud.google.com/security/products/asset-inventory)
- [allAuthenticatedUsers](https://cloud.google.com/iam/docs/overview#all-authenticated-users)


Photo by [Vitalii Onyshchuk](https://unsplash.com/@true_zuzu?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash) on [Unsplash](https://unsplash.com/photos/a-city-at-night-with-a-full-moon-in-the-sky-0cbfix8Y_8g?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash)