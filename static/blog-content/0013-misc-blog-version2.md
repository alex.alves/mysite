## Why, why did I do this?

Sounds a lot lot of pain to try to justify something. I grappled myself with this question **why did I decide to revamp this site**, after all, the original version was quite good, but it stopped fitting my needs and I also wanted to have more control over it. 

With that, I knew what I wanted, I also knew it wouldn't be easy, effectively the last time I did anything remotely similar to Web Development was back in 2012/2014 when I created an incident management the tool with Java Server Faces, Glassfish and PostgressQL to replace the existing tool based on Perl and text files. The effort results was a reductiong of search results from several minutes to less than 10 seconds. Effectivaly, the original version used files for each incident description while my version had that into the database. 

Programming is something I am aware of but not profecient, so anything complex takes me ages, or at least used to take me ages but more on that later in the post.

## Where did I start

My requirements when I started this journey were:
- Obtain more control over the site
- Must be to be deployed as a static website
- Ability to hold multiple resumes
- Possibility to add more layouts as needed

Based on the high-level requirements, I found that SvelteKit to be straight forward along with Typescript. The main reason I decided to go for Typescript is mostly due to preference of enforcing type in my variables or objects, something that stayed with me since my early days in IT.

But the journey was not easy ....

## The process

First were baby steps, reading a lot about Sveltekit, new concepts such as reactivity and Promises, which turned out to be easily relatable with Observer Pattern and somewhat multithreading.  Still what I strugledd most was with the server side vs client side. If you know SvelteKit, the approach is to name the files **+page.svelte** for the visual display, **+page.ts** or **+page.js** for Typescript or Javascript that will executed on the client side and **+page.server.ts** for any code that will run exclusively on the server side. What I was not aware initially is that **+page.ts** the very first time will be executed on the server side and it generated some hiccups along the way.

But this was not all, I am not proficient at Frontend, I used Java Server Pages to create a Fronten that looked like 90s interfaces for typical data access. Thefore, I had to do something and did no want to go down the route of learning CSS or Tailwind or both. In the end, I did learn a bit but more on that in a moment.

The biggest hurdle was how to organize the components, especially that they need to behave differently according to the screen the page is being display at. For that I came across [Flowbite](https://flowbite-svelte.com) which has ready made components, there are other alternatives of course, such as, Skeleton UI or ShadCDN, but Flowbite was what clicked for me. These ready components allowed me to quickly prototype a look, see how I could create buttons, display text, and so forth. 

Still I was struggling to handle the frontend, specifically organizating the components, and reading the about it just made me more confusing at files ... I needed help. 

## My partner in crime

Up to a certain extent, it is great to be living in this age where AI can help you with so many tasks. I've been using [Claude](https://claude.ai) for quite sometime and when they introduced the possibity to create Projects, that was a game changer for me. Perhaps I will create a post about how I use AI ... just a though for the moment.

I could say Claude has became the main developer for this site while I took the role of Product Manager / QA / Tester. I initiate a project in Claude with the info:

```text
All code is in SvelteKit and TypeScript unless specified otherwise.
```

And then I initiated with a simple prompt.

```text
I want to create a new Project to host my Personal Website.
It will have the following sections: 
About Me and Blog
The About Me, will have a section with a introduction about myself, certifications obtained, experience and a link for download role specific resumes.
The Blog Section will have multiple themes, and each theme will have their own set of articles. 
Use Flowbite for the UI components, SvelteKit and typescript. 
Create a MVP
```

Claude generated the code and I told it to make adjustments where needed, such as:

```text
let's focus on the file src/routes/about/+page.svelte
I wan the layout to not use more than 75% width of the screen unless it is a small screen, example mobile.
```

or 

```text
now I want to change this to avoid the usage of Card element.
The About Me will have the intro about me.
Certifications will be a matrix of 4x2 where each cell will have an image of the certification, when it was issued and until when it is valid for.
Leave experience as is for now.
```

These small increments allowed me to progress quickly and towards what I wanted.

## When Claude gets confused

So ... remember when I wrote a few paragraphs earlier "more on that in a moment"? Now is the time to address that. From time to time Claude gets a bit confused and reiterates over the same or very similar approaches, the area where it struggled more was with how to organize the components outside the main layout. For the Blog posts and despite me writing I wanted a 75% width of the display, the rendered component was close to 20% width and not centered. When this situation happened, I had to do the leg work myself by enabling the developer tools, see what was being rendered, which CSS classes were being applied and finally adjust to what I wanted. 

On other occasions, I would simply state what the problem was and it would regenerate the code with logging and debug steps, close to 50% of the times, this approach fixed the issues.

Once the issue was fixed, I would provide Claude with the working code in order to make sure it would not provide me again the non-working version of the component or page.

## What now?

This is now in a state that I like and can I work with independently. There will be some items that require update or improvement but for the moment this is, in my view, better than the previous website version. Maybe not from visual but for sure over control.

Now this will keep evolving as long as my time allows but I can now do the following:
- filter posts by tags
- filter experiences by tags
- see list of post groups by year
- can add multiple resumes (although considering generating one from the select tags)


**Photo by** [Tobias Jetter](https://unsplash.com/@tobi_jet?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash) on [Unsplash](https://unsplash.com/photos/a-beach-with-clear-blue-water-NNWHweF6UGI?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash)

      