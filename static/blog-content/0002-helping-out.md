## Background

I got asked by an acquaintance if I could help his startup in getting a test environment and CI/CD in place. The ask mainly came over as this person noticed that I had the AWS Solution Architect certification. During initial assessment and initial preparation, it rapidly transpired that they did not track the resources in AWS and were also complaining about cost increase.
At the time they were facing 165% costs compared to when they launched.

## Bringing the costs down

As the environment was small, the quick way to verify what was increasing the bill was to simply use the [AWS Pricing calculator](<https://calculator.aws/>) and see if the cost estimation matched what they were being charged. Indeed they were being charged approximatly **65%** more than they should.
Once I got this information I asked for a detailed bill and also read-only access to their billing console. This is a two step activity:
1. The root user must provide [access to the Billing Console](<https://docs.aws.amazon.com/awsaccountbilling/latest/aboutv2/control-access-billing.html#ControllingAccessWebsite-Activate>)
2. The IAM policy must be updated to allow the user access to the Billing Console(<https://docs.aws.amazon.com/awsaccountbilling/latest/aboutv2/billing-permissions-ref.html>) with the necessary permissions.

Once I got access and looked at the detailed billing I noticed a couple of resources in regions that are not in use. Once I confirmed they were not in use, I  proceeded with their deletion. This simple exercise, brought the cost down by 65%.

The cost optimization happens all the time. We can implement a Budget and get notified when a certain threshold is crossed and trigger a look at what is consuming more than usual. Alternatively, AWS Config provides continous monitoring of AWS resources triggering notification when a change occurs in the monitored resources.

**Photo by** [Gerd Altmann](https://pixabay.com/users/geralt-9301/?tab=about) on [Pixabay](https://pixabay.com/)