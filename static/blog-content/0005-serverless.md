Serverless computing is a paradim that has been growing rapidly in the cloud space. Similar to PaaS, development teams do not have to manage infrastructure components such as network, servers, storage. They can focus on writing code and deploying to a serverless offering that will scale up and down automatically and run when needed.

## What is serverless computing?

Starting with the misleading name "serverless", it does not mean servers are not involved instead the infrastructure that includes the servers is managed by the cloud vendor. The organization ends by paying for the resources it actually uses. 

Teams can deploy small, self-contained applications that are executed when needed. An example is when software teams deploy event driven applications where certain code gets executed as an event gets triggered. Another example is an API that will only run the code when its endpoint gets called which will trigger the spin up of a container to run the API code.


## How does serverless computing work?

As mentioned initially, the cloud vendor is reponsible for managing:
- Server infrastructure:
  - Operating system installation and patching,
  - Server Virtualization software,
  - Orchestration software,
  - Backup and restores of the OS and Virtualization software
- Network infrastructure:
  - Switches,
  - Routers,
  - Firwmare updates

All of these items must run with enough redundancy to allow a consistent behaviour when organizations use their serverless offerings to execute their applications. The developer teams will focus mostly on the code and deploying it into the serverless offering.

Typically, when the code is executed, the cloud vendor will register for how long and what amount of resources where consumed. This will then be charged to the organization.

## Why is serverless popular?

A number of reasons made it popular. Let's look from an organization perspective:

| Capability | Traditional | Serverless |
|---|---|---|
| Elasticiy| Does not scale up and down easily | Limited by the service offering, typically in the hundreds instances|
| Costing | Fixed server cost plus DC hosting plus utilities | Pay for the time and resource usage |
| Lead time | Typically 3 to 6 months | Minutes for initial setup, seconds afterwards |
| Lifecycle | Managed by the organization | Managed by the cloud vendor and transparent to the organization |
| Availability | Subject to organization planning | handled by the cloud vendor and typicall in the 99.99% |

There are plenty of reasons in favour, although with serveless there is less control over configurations and sometimes might require tuning or reaching out to the vendor to get the right parameters for the service.

A good example while looking at Azure or Google Cloud, if you wish to use serverless you are stuck with HTTPS protocol. In the event you need to handle TCP or UDP stream you will need to consider something else either Virtual Machines or Kubernetes clusters.


Photo by [Adi Goldstein](https://unsplash.com/@adigold1?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText) on [Unsplash](https://unsplash.com/photos/EUsVwEOsblE?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText)  