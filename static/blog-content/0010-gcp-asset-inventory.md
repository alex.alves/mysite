## Overview

Security is always a hot topic and although we can secure each solution on a one by one basis, we can help application and infrastrcture teams by offloading some of these concerns. With the implementation of Google Cloud Organization policies we aim exactly at enabling a secure posture within a Google Cloud Organization.

## What are Organization Policies

Policies or as per Google Cloud documentation, constraints, are configurations that limit what can or cannot be done within a Google Cloud Organization, Folder or Project.

Dependending on the type of policy, they can block entirely the usage of a service, limit where resources can be deployed, or even the allowed configuration of a given service.

The policies are available at the organization level and can be enforced at Organization, Folder or Project level.

The way Organization Policies work is via the hierarchal resource assignment. A policy assigned at Organization level is inherited by its children, unless the policy is overwritten at a lower level.

Figure 1 demonstrates an example. 
1. Policy *constraints/sql.restrictPublicIp* is applied at Org level
2. Non-prod folder has an exemption resulting in children projects being exempt of the policy
3. Project C has an exemption.

{{< figure src="images/org-policies-inherance.png" title="Figure1 - Polices inherance" class="center">}}

### Standard Organization Policies

These are the policies that are provided by Google Cloud and they cover [several services](https://cloud.google.com/resource-manager/docs/organization-policy/org-policy-constraints)
These policies contain two constraint types:

- boolean: Basically it is either the policy is applied or not applied. See [here](https://cloud.google.com/resource-manager/docs/organization-policy/using-constraints#boolean-constraint) for a full updated list.
- list: The constraint receives a list of values and these can be allowed or denied. See [here](https://cloud.google.com/resource-manager/docs/organization-policy/using-constraints#list-constraint) for a full updated list.

An example: 
- boolean: the policy **constraints/sql.restrictPublicIp** which restrics the Cloud SQL services from having public IPs assigned to it is of type boolean. Either it is allowed or denied.
- list: the policy **constraints/compute.requireVpcFlowLogs** which forces VPCs to have flow logs enabled, receives a list of the allowed configurations for the flow logs. Setting this constraint with ESSENTIAL, LIGHT, and COMPREHENSIVE would allow any configuration of flow logs except disabling them.

### Custom Organization Policies

These policies are recent and provide a way to further extend what users can or cannot do in Google Cloud.
As of today there are a few subset of services that can be covered by custom policies. At the time of writing, the following services are supported:
- Cloud Firewall
- Compute Engine
- Google Kubernetes Engine
- Virtual Private Cloud
- Dataproc

These custom policies allow for a finer grained control over the supported resources and are an extension of the standard policies.

For a full list see [Supported services for Custom Contraints](https://cloud.google.com/resource-manager/docs/organization-policy/custom-constraint-supported-services).

#### What value do they bring

Due to the nature of the supported services and because the custom contraints use a CEL expression for evaluation, they provide a finer grain of control. 

##### Firewall related
Additionally, they help in ommiting alerts in other services. 

An organization that makes use of the hierarchal firewall can define a rule at the org level blocking the incoming traffic from 0.0.0.0/0 for protocol HTTP (port 80). This will effectively block any project from being able to receive incoming communication on port 80. However, this does not block teams from creating a project level firewall rule for accepting incoming traffic on port 80 from 0.0.0.0/0. The project users will never see their firewall rule being hit, this is due to the way how the firewall evaluation works, more info about firewall [here](https://cloud.google.com/firewall/docs/firewall-policies-overview) and the [evaluation flowchart](https://cloud.google.com/static/firewall/images/firewall-policies/hfw3-2.svg)

For those organization using Security Command Center (SCC), they will receive a finding that port 80 is open to 0.0.0.0/0 even though the port is blocked at the hierarchal firewall, but because project users can still create the rule, it still flags it, despite being a false finding.

##### Compute Engine related

An organization might want to limit the images that Compute Engines can use, until now, the only way to limit this was to create a project and create "custom" images then use the standard constraint constraints/compute.trustedImageProjects to allow compute engine to use only images from the allowed project.

With the custom constaint, it is possible to create an expression to allow only specific images or block specific images.

## Recommended Policies

I recommend organization policies as a starting point:

| constraint                       | What does it do         | Why should be applied |
|----------------------------------|-------------------------|-----------------------|
|constraints/sql.restrictPublicIp  |Blocks Cloud SQL from having public IP| Databases should not be directly exposed to the internet|
|constraints/compute.vmExternalIpAccess| Blocks VMS from having public IP| VMs should not be directly exposed to the internet. This constraint also affects other services that rely on VMs, such as, GKE, Vertex, Dataflow|
|constraints/iam.workloadIdentityPoolProviders|Limits with who federation can be established|Federation should be enabled with well known and "trusted" providers|
|constraints/gcp.restrictTLSVersion|Limits which TLS version are allowed|Forces services to use the version specified or above, therefore blocking unsecure TLS versions|
|constraints/gcp.restrictServiceUsage|Blocks APIs from being used|A good behavior to avoid surprise bills as users cannot use the services if they are in the constraint list|
|constraints/iam.allowedPolicyMemberDomains|Limits the allowed principals to those in the Google Identity or Google Workspace|To avoid users using their personal accounts, or avoid a rogue agent from getting access to the platform|


## Applying exemptions

Certain times, there might be the need to add an exemption for a given policy for a given project or folder.
The exemption can be achieved via two different methods.
- Direct exemption
- Conditional exemption

### Direct Exemption

By direct exemption, I mean that we overwrite the polciy details on the target resource. Using the Figure 1 example, the  **constraints/sql.restrictPublicIp** is applied at the organization level. The folder non-prod is then overwritten to allow Cloud SQL instances to have public IP. The result is that any Cloud SQL deployed in a project below the non-prod Folder can have a public IP.

*Note*: When using Terraform, there is a risk that terraform will fail to deploy when a project is delete, this is because of the direct assignment and is registers as this contrainst is tied up to project X. To fix it, Terraform state file must be modified to remove that reference.

### Conditional Exemption

It is possible to use TAGs to enforce or exempt policies. TAGs themselves are a type of resource that get attached to another resource.
TAGs, like Organization Policies, are inherited by the children resources. 
The idea with using tags is we can use them to make conditions on the policies. The pitfall with Terraform no longer exists and finding which resources are exempted becomes easier as we can simply query the Asset Inventory for the bindings that a given TAG has.
Moreover, a single TAG can waive multiple constaints. 


### Direct vs Conditional

Best is to use an example to see the main difference between enforcing/exempting policies.

**Example case**:

Organization my.example.com blocks all Cloud Storage buckets from using HTTP and unenthenticated access by enforcing the following two policies:
- constraints/storage.publicAccessPrevention = True
- constraints/storage.secureHttpTransport = True

A team produces public artifacts and dumps them into a Cloud Storage, the artifacts are not retricted and are to be consumed by anyone.

Using the direct exemption approach, this means that for the given Cloud Storage bucket, both constraints would need to be overwritten, by setting them to False, in order allow the public IAM policy and allow HTTP protocol.

Using Tags we could have:

```shell
  TAG Key = "allowPublicAccess"
  TAG Value = "true"
```

*Note*: Tags **need** to have at least one value.

When enforcing the constraints, they would have a condition attached to them along the lines of:
```shell
"!resource.matchTag('allowPublicAccess', 'true')"
```

The Cloud Storage bucket would need to have only one TAG and would be exempted from both policies, allowing the Cloud Storage to be publicaly accessible.

## To Summarize

This post explores the significance of Google Cloud Organization Policies in establishing a secure cloud environment. By leveraging standard and custom policies, organizations can control and limit actions across their Google Cloud platform, enhancing security and compliance. I've highlighted recommended policies for securing cloud resources and discussed the strategic use of exemptions to balance security needs with operational flexibility. 

At the end, effective implementation of these policies enables organizations to automate security and compliance, ensuring a robust and secure cloud ecosystem.


## References
- [Standard Policies](https://cloud.google.com/resource-manager/docs/organization-policy/org-policy-constraints)
- [Boolean Constraints](https://cloud.google.com/resource-manager/docs/organization-policy/using-constraints#boolean-constraint)
- [List Constraints](https://cloud.google.com/resource-manager/docs/organization-policy/using-constraints#list-constraint)
- [Supported services for Custom Contraints](https://cloud.google.com/resource-manager/docs/organization-policy/custom-constraint-supported-services)
- [Creating and managing Policies](https://cloud.google.com/resource-manager/docs/organization-policy/creating-managing-policies)
- [Managing Policies with TAGs](https://cloud.google.com/resource-manager/docs/organization-policy/tags-organization-policy)



  
Photo by [Florian Wehde](https://unsplash.com/@florianwehde?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash) on [Unsplash](https://unsplash.com/@florianwehde?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash)
  