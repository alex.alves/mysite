**NOTE:** Google Cloud has released PAM (Privileged Access Manager) that replaces JIT. Also JIT is now called JIT-Group, more info [here](https://github.com/GoogleCloudPlatform/jit-groups).

## Overview

Best practices state that you should have the privileges to do your function no more and no less. A few times, these privileges are still to high and one of the common "workarounds" is to have a separate set of credentials for those high-privileged accounts. This brings the downside of having to remember two or more set of credentials and human nature tends to use the same password or a variation of the original password, increasing the risk that one or more accounts belonging to a user get compromised.

Therefore, **Just in Time Access** was created. It is a piece of software created by the Google Cloud community. It allows you configure users with higher privileges to request it on demain instead of having it at all times.

## Why Just In Time?

Within the usage of Google Cloud, certain users will require higher privleges, that might be users that require administrative rights, such as, Organization Administrator or Security Admins. To refresh, a role can be provided at 4 different levels, resource, project, folder and organization. A role given at organization level will have access to all folder, projects and resources beneath it. 

As an exampple, security team must act quickly in the event of a security incident. The security analyst would need to block a communication, or disable a Compute Engine. Instead of providing this, a permanent privelege to an "higher-privileged" account at folder or organization level, the same privileged can be provided to the normal account. The analyst would then request higher privileges which would be given for a X amount of time on a given project.

## How is access provided?

Google Cloud IAM allows for roles to be given to users under certain conditions. Just in Time Access leverages this feature to provide its functionality. and uses it to generate dynamic conditions when a user requests a given role.

For each role that Just In Time will handle access to, it will need to have a condition attached to the principal, either a direct user or a group.

The roles can be provided two ways:
1. Self-approved: The user can request a given role and it is self-approved the conditon is: ```has({}.jitAccessConstraint)```
2. Multi-party approval: The user can request a given role and who needs to approve. Access request is valid for a period of time. The condition is: ```has({}.multiPartyApprovalConstraint)```

## The Downside

At the time of writing, Just in Time version available is 1.4.1. This version does not allow  use for different teams as the condition will always be the same.


## Deployment

The developers provide guided deployments for AppEngine and Cloud Run. If you want to rely on it, you can make use of the load balancer and deploy the service across a number of regions. Perhaps even protecting it further by including Cloud Armor along side with the Load Balancer.

I'll be covering the setup of Cloud Run behind a Global Load Balancer in a different post. 

## Reference documentation
- Github repo: https://github.com/GoogleCloudPlatform/jit-access
- Google Cloud Blog: https://cloud.google.com/architecture/manage-just-in-time-privileged-access-to-project
 
  
Photo by [Everaldo Coelho](https://unsplash.com/@_everaldo?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText) on [Unsplash](https://unsplash.com/photos/qYdUUZVWiVY?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText)