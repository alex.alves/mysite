## Overview
In this post, we delve into Workforce Identity Federation, a service that enables organizations to grant access to Google Cloud services without the need for users to create a Google Account. This simplifies user management and enhances security by leveraging existing identity providers.

While Workforce Identity Federation shares some similarities with Workload Identity Federation, they serve different purposes. Workforce Identity Federation focuses on providing access for employees and users, whereas Workload Identity Federation is designed for managing access for service accounts and applications.

## Diference between federations

Google Cloud has two similar named services:
-. Workfload Identity Federation
-. Workforce Identity Federation

And additionally to this there is also another way to federate users using [Google Cloud / G Suite Connector by Microsoft](https://azuremarketplace.microsoft.com/en-us/marketplace/apps/aad.googleapps); this is not a Google offering.

### Workload Identity Federation

This service targets application to application communication. When configuring the Workload Identity Federation, we need to specify a service account (recommended) that will act on behalf of the caller. This also means, that several workloads can act as a single service account.

### Workforce Identity Federaration

This service targets user access, there will no equivalent user in Google Gloud. The way it works is that it delegates the authentication to the service provider and creates a metadata entry in Google Cloud for the given user. What it really means, is that you do not need to sync your users to Cloud Identity or create Google Accounts for those users.

### G Suite Connector by Microsoft

This service syncs users and groups to Cloud Identity or Google Workspace. Effectively, it pull users from the configured IDP and creates Google Accounts for them.

#### Service Comparison

I'll focus on the federation with Azure Entra ID (former Azure Active Directory ).

| Feature / Capability                    | Workload | Workforce | GCDS |
|-----------------------------------------|:--------:|:---------:|:----:|
| Creates new Google Account              | NO       |  NO       | YES  | 
| Synchronizes AD Security Groups         | NO       |  YES      | NO   |
| Synchronizes Office 365 Groups          | NO       |  YES      | YES  |
| Synchronizes Distributions List         | NO       |  YES      | NO   |
| Automatically deletes accounts          | NO       |  NO       | YES  |
| Requires Entra ID App or Enterprise APP | YES      |  YES      | YES  |
| Supports SAML                           | NO       |  YES      | YES  |
| Supports OIDC                           | NO       |  YES      | NO   |
| Configuration available org wide        | NO       |  YES      | NO   |


## Limitations of Workforce

It provides an easy integration for organizations to use, although with same caveats:
1. After the initial configuration, any changes to the mapping, will require existing users to be purged from Google Cloud for the mapping to take place, new users take the new mapping,
2. The link to login to Google Cloud Console is specific, you cannot use https://console.cloud.google.com for federated users,
3. Once a user is disabled or removed from the Identity Provider, the user metadata must be manually deleted in Google Cloud,
4. There is no way to view the list of users that have logged in via Federation to make the deletion easier,
5. There is a limit of 4kb for the attributes mapping,
6. To setup Federation, it is required to have IAM organization level access,
7. Providing access to groups, uses the group ID and not the human readable name.

**NOTE:** Point 7 is one of those that can be very tricky in incident resolution due to the groupId. Example: while using Google Groups or Synchronized Office 365 Groups, we would provide the role of Cloud Run Viewer with group_email@org.com, with Workforce federation the same group would look like this; principalSet://iam.googleapis.com/locations/global/workforcePools/**pool_id**/group/1231a342s-12312312-123124asd-1a2b3c1d. 
This is obviously not human readable at all, making analysis a lot harder when assessing accesses in a non-programmatic way.

## Summary

I've briefly covered the possibilities to federate your users and workloads, then focused on comparing the differences between the services and lastly, I focused on Workforce. Even with the limitations, some of these can be bypassed to improve the logging and access experience.
The official documentation advices to make google.sub equal to assertion.subject, this mapping means that user1@org.com will be visibale in the logs as principal://iam.googleapis.com/locations/global/workforcePools/WORKFORCE_IDENTITY_POOL_ID/subject/**randomstring**", by changing this mapping from google.sub = assertion.subject to google.sub = assertion.email, we now can get in the logs principal://iam.googleapis.com/locations/global/workforcePools/WORKFORCE_IDENTITY_POOL_ID/subject/**user1@org.com**.

Although not covered here, the access can also be done via specific token attributes.

## References

- [Overview of Google Cloud Workload Federation](https://cloud.google.com/iam/docs/workforce-identity-federation)
- [Configure Workforce Identity Federation with Azure Entra ID](https://cloud.google.com/iam/docs/workforce-sign-in-azure-ad)
- [Deleting Users in Workforce Identity Federation](https://cloud.google.com/iam/docs/workforce-delete-user-data)
- [G Suite Connector by Microsoft](https://cloud.google.com/architecture/identity/federating-gcp-with-azure-active-directory#implementing_federation)


**Photo by** [Adrian Villa](https://unsplash.com/@adrivillab?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash) on [Unsplash](https://unsplash.com/photos/a-castle-with-a-steeple-on-top-of-a-hill-c0sPzuc9KZ4?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash)
  