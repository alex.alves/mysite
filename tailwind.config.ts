import flowbitePlugin from 'flowbite/plugin'

/** @type {import('tailwindcss').Config} */
// import type { Config } from 'tailwindcss';

export default {
	content: ['./src/**/*.{html,js,svelte,ts}', './node_modules/flowbite-svelte/**/*.{html,js,svelte,ts}'],
	darkMode: 'selector',
	theme: {
		extend: {
			colors: {
				// Main colors - Shifted to deeper blues
				primary: {
				  50: '#edf5ff',
				  100: '#d6e8ff',
				  200: '#add1ff',
				  300: '#7ab4ff',
				  400: '#4b95ff',
				  500: '#1c7aff', // More vibrant blue primary
				  600: '#0062e6',
				  700: '#0051c3',
				  800: '#00429d',
				  900: '#003782',
				},
				// Secondary accent color - Shifted to cooler tones
				accent: {
				  50: '#e9f6ff',
				  100: '#d0eaff',
				  200: '#a6d5ff',
				  300: '#73b8ff',
				  400: '#4098fc',
				  500: '#2079f3', // More saturated blue accent
				  600: '#1266e4',
				  700: '#0e55c8',
				  800: '#0e47a1',
				  900: '#103c7e',
				},
				// Background colors - Slightly cooler tints
				background: {
				  light: '#ffffff',
				  dark: '#102040', // Deeper blue-based dark
				  alt: '#f0f7ff', // Subtle blue tint
				},
				// Text colors - Slightly adjusted for blue theme
				content: {
				  primary: '#12253a', // Bluer text
				  secondary: '#3a5275', // Bluer secondary
				  light: '#8ca3c9' // Bluer light text
				}
			  }
		}
	},

	plugins: {
		"@tailwindcss/postcss": {},
	}
};