import type { Certification } from '$lib/objects/Certification';
import type { CompanyExperience } from '$lib/objects/Experience';
import YAML from 'yaml';

export async function loadCertifications(): Promise<Certification[]> {
  const response = await fetch('/aboutme/certifications.yaml');
  const text = await response.text();
  const data = YAML.parse(text);
  return data.certifications;
}

export async function loadExperiences(): Promise<CompanyExperience[]> {
  const response = await fetch('/aboutme/experiences.yaml');
  const text = await response.text();
  const data = YAML.parse(text);
  return data.experiences;
}

