export interface BlogPost {
    id: string;
    title: string;
    summary: string;
    date: string;
    author: string;
    coverImage?: string;
    tags: string[];
}

export const blogPosts: BlogPost[] = [
    {
        id: "0001-building-this-website",
        title: "How this website came to be",
        summary: "A brief overview of my journey to build a portfolio site.",
        date: "2021-10-01",
        author: "Alexandre Alves",
        coverImage: "/images/posts/0001-martin.jpg",
        tags: ["fun", "web", "development"]
    },
    {
        id: "0002-helping-out",
        title: "Optimizing AWS infra for a small start up",
        summary: "How I used the best practices to reduce costs by 30%",
        date: "2021-11-15",
        author: "Alexandre Alves",
        coverImage: "/images/posts/0002-gears_and_time.png",
        tags: ["cloud", "aws", "cost"]
    },
    {
        id: "0003-cloud-computing",
        title: "Cloud Computing and their types",
        summary: "A brief overview between the cloud computing and the multiple types, IaaS, PaaS, SaaS",
        date: "2023-03-01",
        author: "Alexandre Alves",
        coverImage: "/images/posts/0003-felix-mittermeier.jpg",
        tags: ["cloud"]
    },
    {
        id: "0004-private-public-cloud",
        title: "Public vs private vs Hybric vs Multicloud",
        summary: "A brief overview between the cloud computing and the multiple types, IaaS, PaaS, SaaS",
        date: "2023-03-08",
        author: "Alexandre Alves",
        coverImage: "/images/posts/0004-sigmund.jpg",
        tags: ["cloud"]
    },
    {
        id: "0005-serverless",
        title: "Serverless",
        summary: "A brief overview about Serverless",
        date: "2023-03-15",
        author: "Alexandre Alves",
        coverImage: "/images/posts/0005-adi-goldstein.jpg",
        tags: ["cloud"]
    },
    {
        id: "0006-gcp-terraform-iam",
        title: "Terraform IAM Resources for Google Cloud",
        summary: "The different options for IAM when using Terraform with Google Cloud Provider",
        date: "2023-08-15",
        author: "Alexandre Alves",
        coverImage: "/images/posts/0006-astronomy.jpg",
        tags: ["cloud", "terraform", "iam", "gcp", "IaC", "security"]
    },
    {
        id: "0007-gcp-jit",
        title: "Just in Time for Google Cloud",
        summary: "An overview of what Just in Time is, what it is for and how to deploy it.",
        date: "2023-09-15",
        author: "Alexandre Alves",
        coverImage: "/images/posts/0007-everaldo-coelho.jpg",
        tags: ["cloud", "terraform", "iam", "IaC", "gcp"]
    },
    {
        id: "0008-gcp-cr-and-lb",
        title: "Cloud Run and Load Balancer deployment",
        summary: "How to deploy a Cloud Run service behind a Load Balancer",
        date: "2023-11-13",
        author: "Alexandre Alves",
        coverImage: "/images/posts/0008-martin-krchnacek.jpg",
        tags: ["cloud", "terraform", "IaC", "gcp"]
    },
    {
        id: "0009-gcp-asset-inventory",
        title: "Google Cloud Asset Inventory",
        summary: "Asset Inventory is a powerful tool to check your Google Cloud project(s), folder(s), and organization resources without direct access.",
        date: "2023-12-15",
        author: "Alexandre Alves",
        coverImage: "/images/posts/0009-vitalii-onyshchuk.jpg",
        tags: ["cloud", "sql", "inventory", "gcp"]
    },
    {
        id: "0010-gcp-asset-inventory",
        title: "Securing your Google Cloud with Organization Policies",
        summary: "How to improve your Google Cloud security posture via Organization Policies",
        date: "2024-02-03",
        author: "Alexandre Alves",
        coverImage: "/images/posts/0010-florian-wehde.jpg",
        tags: ["cloud", "sql", "inventory", "gcp", "security"]
    },
    {
        id: "0011-gcp-scc-sec-posture",
        title: "Securing your Google Cloud with Security Posture (SCC)",
        summary: "An Overview of Security Command Center Security Posture feature.",
        date: "2024-02-19",
        author: "Alexandre Alves",
        coverImage: "/images/posts/0011-szymon-shields.jpg",
        tags: ["cloud", "sql", "inventory", "gcp", "security"]
    },
    {
        id: "0012-gcp-azure-federation",
        title: "Enabling Federation between Google Cloud and Azure Entra ID",
        summary: "An Overview of Security Command Center Security Posture feature.",
        date: "2024-09-10",
        author: "Alexandre Alves",
        coverImage: "/images/posts/0012-adrian-villa.jpg",
        tags: ["cloud", "federation", "iam", "gcp", "azure"]
    },
    {
        id: "0013-misc-blog-version2",
        title: "A refresh was due",
        summary: "My journey to revamp  my site in order to obtain same more control",
        date: "2025-03-09",
        author: "Alexandre Alves",
        coverImage: "/images/posts/0013-tobias-jetter.jpg",
        tags: ["misc", "fun", "nodejs", "svelte", "sveltekit"]
    },
];