export interface Position {
    title: string;
    startDate: string;  // Format: 'YYYY-MM'
    endDate: string;    // Format: 'YYYY-MM' or 'Present'
    description: string;
    tags: string[];
}

export interface CompanyExperience {
    company: string;
    startDate: string;  // Format: 'YYYY-MM'
    endDate: string;    // Format: 'YYYY-MM' or 'Present'
    positions: Position[];
}