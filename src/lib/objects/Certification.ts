export interface Certification {
    name: string;
    issuer: string;
    infoUrl?: string;
    imageUrl: string;
    issueDate: string;
    validUntil: string;
}
