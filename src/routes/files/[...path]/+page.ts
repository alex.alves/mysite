

const staticFiles = [
    'examples/terraform/0011-scc-security-posture-essentials.tf',
    // Add all your static files here
];

import { error } from '@sveltejs/kit';

export const entries = () => staticFiles.map(path => ({ path }));

export async function load({ params }) {
    // During build, only prerender the known static files
    if (!staticFiles.includes(params.path)) {
        throw error(404, 'Not found');
    }
    
    return {
        path: params.path
    };
}

