import { error } from '@sveltejs/kit';
import { blogPosts } from '$lib/data/blog-data';
import type { PageLoad } from '../$types';

export const prerender = true;

export const load: PageLoad = ({ params }) => {
    const post = blogPosts.find(p => p.id === params.slug);
    if (!post) {
        throw error(404, 'Post not found');
    }
	return { post }
};